import { useNavigate } from "react-router-dom";
import keyboard from "../keyboard.png";
import arrow from "../arrow.png";
import { useState } from "react";
import ShowImages from "./ShowImages";

const Translation = () => {
    const [keyword,setKeyword] = useState("");
    const name = localStorage.getItem('name');
    const navigate = useNavigate();
    const search = (e) => {
        console.log("name :",keyword);
    };

    if (name) {
        return(
            <div>
                <div style={{background : "#FFC75F"}}>   
                                   
                    <div style={{display: "flex",flexDirection : "row",justifyContent: "center",alignItems:"center",height : "300px"}}>
                        
                        <div 
                                style={{
                                    borderRadius: "50px",
                                    border: "2px solid ",
                                    paddingLeft: "20px",
                                    width: "60%",
                                    height: "40px",
                                    background :"#EFEFEF"
                                }}  
                        >
                                <img src={keyboard} alt="Keyboard" style={{width:"5%"}}/>
                                <input
                                    style={{
                                        border: "none transparent",
                                        outline: "none",
                                        background : "#EFEFEF"
                                    }}
                                    placeholder="Search value"
                                    onChange={(e) =>
                                        setKeyword(e.target.value)
                                    }
                                />

                                <button 
                                    style={{
                                        background : "#845EC2",
                                        borderRadius: "50px",
                                        height: "30px",
                                        width: "30px",
                                        float : "right",
                                        margin :"5px"
                                    }}
                                    type="button"
                                    onClick={search}  
                                >
                                    
                                    <img style ={{width :"10px",height:"10px"}} src={arrow} alt="button" />
                                </button> 
                        </div>

                    </div>       
                </div> 
                         
                <ShowImages word={keyword} />
            </div>

        )
    }
    else{
        navigate("/");
    }
    
}

export default Translation;