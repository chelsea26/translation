import logo from "../logo.png"
const Header = (props) => {
    return(
        <div>
            <div style={{
                    background : "#FFC75F",
                    height : "50px",
                    boxSizing: "border-box",
                    display: "flex",
                    flexDirection : "row",
                    paddingLeft : "20%",
                    justifyContent: "left",
                    alignItems: "left"        
                }}>
                    {props.logo ? <img alt="logo" src={logo} style={{width :"4%"}} />  : ""}
                    <p> Lost In Translation </p>
                    <p style={{justifyContent: "right",alignItems: "right"}}> Username </p>
            </div>

            <div style={{borderTop: "3px solid #bbb"}}>
                
            </div> 


        </div>
        
    )
}

export default Header;