import logo from "../logo.png";
import keyboard from "../keyboard.png";
import arrow from "../arrow.png";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const Signin = () => {

    let navigate = useNavigate();
    const [name,setName] = useState("");
    const sign = (e) => {
        localStorage.setItem('name', name);
        console.log("name :",name);
        navigate("/translation");

    };
    return(
        <div>
            <div style={{background : "#FFC75F"}}>
                
                
                <div style={{display: "flex",flexDirection : "row",height : "300px"}}>

                    <img alt="logo" src={logo} style={{width :"15%",padding : "50px",paddingLeft:"20%"}} />

                    <div style={{ justifyContent: "center",padding:"10%"}}>
                        Lost In Translation
                        <br/><br/>
                        Get Started
                    
                    </div>

                    
                </div>


                
            </div>

            <div style={{paddingLeft :"10%",
                            width : "80%"
                            }}>

                    <div style={{
                            borderRadius: "25px",
                            border: "2px solid",
                            borderBottom : "20px solid #845EC2",
                            paddingLeft: "20%",
                            width: "80%",
                                
                    }}>
                        <br/><br/>
                        
                       <div 
                            style={{
                                borderRadius: "50px",
                                border: "2px solid ",
                                paddingLeft: "20px",
                                width: "60%",
                                height: "40px"
                            }} 

                          
                        >
                            <img src={keyboard} alt="Keyboard" style={{width:"5%"}}/>
                            <input
                                style={{
                                    border: "none transparent",
                                    outline: "none",
                                    paddingLeft : "50px"
                                }}
                                placeholder="Whats Your Name?"
                                onChange={(e) =>
                                    setName(e.target.value)
                                  }
                            />

                            <button 
                                style={{
                                    background : "#845EC2",
                                    borderRadius: "50px",
                                    height: "30px",
                                    width: "30px",
                                    float : "right",
                                    margin :"5px"
                                }}
                                type="button"
                                onClick={sign}
                                
                            >
                                
                                <img style ={{width :"10px",height:"10px"}} src={arrow} alt="button" />
                            </button>
                           
                            
                            
                        </div>
                        
                        

                        <br/><br/>
                    </div>
                
            </div>
        </div>
    )
}

export default Signin;