// import logo from './logo.svg';
// import './App.css';
import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Header from "./MyComponents/Header";
import Signin from "./MyComponents/Signin";
import Translation from "./MyComponents/Translation";

function App() {
  return (
    <div>
      <Router>
        <Routes>
            
            <Route
              exact
              path="/"
              element={
                <div>
                 <Header logo={false} />
                 <Signin />
                </div>
              }
            />

            <Route
              exact
              path="/translation"
              element={
                <div>
                 <Header logo={true} />
                 <Translation />
                </div>
              }
            />


            <Route
              exact
              path="/profile"
              element={
                <div>
                  hello Ok
                </div>
              }
            />

          </Routes>
        </Router>
    </div>
  );
}

export default App;
